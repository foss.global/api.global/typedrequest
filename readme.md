# @apiglobal/typedrequest
make typed requests towards apis

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@apiglobal/typedrequest)
* [gitlab.com (source)](https://gitlab.com/apiglobal/typedrequest)
* [github.com (source mirror)](https://github.com/apiglobal/typedrequest)
* [docs (typedoc)](https://apiglobal.gitlab.io/typedrequest/)

## Status for master

Status Category | Status Badge
-- | --
GitLab Pipelines | [![pipeline status](https://gitlab.com/apiglobal/typedrequest/badges/master/pipeline.svg)](https://lossless.cloud)
GitLab Pipline Test Coverage | [![coverage report](https://gitlab.com/apiglobal/typedrequest/badges/master/coverage.svg)](https://lossless.cloud)
npm | [![npm downloads per month](https://badgen.net/npm/dy/@apiglobal/typedrequest)](https://lossless.cloud)
Snyk | [![Known Vulnerabilities](https://badgen.net/snyk/apiglobal/typedrequest)](https://lossless.cloud)
TypeScript Support | [![TypeScript](https://badgen.net/badge/TypeScript/>=%203.x/blue?icon=typescript)](https://lossless.cloud)
node Support | [![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
Code Style | [![Code Style](https://badgen.net/badge/style/prettier/purple)](https://lossless.cloud)
PackagePhobia (total standalone install weight) | [![PackagePhobia](https://badgen.net/packagephobia/install/@apiglobal/typedrequest)](https://lossless.cloud)
PackagePhobia (package size on registry) | [![PackagePhobia](https://badgen.net/packagephobia/publish/@apiglobal/typedrequest)](https://lossless.cloud)
BundlePhobia (total size when bundled) | [![BundlePhobia](https://badgen.net/bundlephobia/minzip/@apiglobal/typedrequest)](https://lossless.cloud)
Platform support | [![Supports Windows 10](https://badgen.net/badge/supports%20Windows%2010/yes/green?icon=windows)](https://lossless.cloud) [![Supports Mac OS X](https://badgen.net/badge/supports%20Mac%20OS%20X/yes/green?icon=apple)](https://lossless.cloud)

## Usage

Use TypeScript for best in class intellisense.

```typescript
import { expect, tap } from '@pushrocks/tapbundle';
import * as smartexpress from '@pushrocks/smartexpress';

import * as typedrequest from '../ts/index';

let testServer: smartexpress.Server;
let testTypedHandler: typedrequest.TypedHandler<ITestReqRes>;

// lets define an interface
interface ITestReqRes {
  method: 'hi';
  request: {
    name: string;
  };
  response: {
    surname: string;
  };
}

tap.test('should create a typedHandler', async () => {
  // lets use the interface in a TypedHandler
  testTypedHandler = new typedrequest.TypedHandler<ITestReqRes>('hi', async (reqArg) => {
    return {
      surname: 'wow',
    };
  });
});

tap.test('should spawn a server to test with', async () => {
  testServer = new smartexpress.Server({
    cors: true,
    forceSsl: false,
    port: 3000,
  });
});

tap.test('should define a testHandler', async () => {
  const testTypedRouter = new typedrequest.TypedRouter(); // typed routers can broker typedrequests between handlers
  testTypedRouter.addTypedHandler(testTypedHandler);
  testServer.addRoute(
    '/testroute',
    new smartexpress.HandlerTypedRouter(testTypedRouter as any) // the "any" is testspecific, since smartexpress ships with its own version of typedrequest.
  );
});

tap.test('should start the server', async () => {
  await testServer.start();
});

tap.test('should fire a request', async () => {
  const typedRequest = new typedrequest.TypedRequest<ITestReqRes>(
    'http://localhost:3000/testroute',
    'hi'
  );
  const response = await typedRequest.fire({
    name: 'really',
  });
  console.log('this is the response:');
  console.log(response);
  expect(response.surname).to.equal('wow');
});

tap.test('should end the server', async () => {
  await testServer.stop();
});

tap.start();
```

## Contribution

We are always happy for code contributions. If you are not the code contributing type that is ok. Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: You can [contribute one time](https://lossless.link/contribute-onetime) or [contribute monthly](https://lossless.link/contribute). :)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
