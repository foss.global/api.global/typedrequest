/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@api.global/typedrequest',
  version: '3.0.1',
  description: 'make typed requests towards apis'
}
