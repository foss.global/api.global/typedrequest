// apiglobal scope
import * as typedRequestInterfaces from '@api.global/typedrequest-interfaces';

export { typedRequestInterfaces };

// pushrocks scope
import * as isounique from '@push.rocks/isounique';
import * as lik from '@push.rocks/lik';
import * as smartdelay from '@push.rocks/smartdelay';
import * as smartpromise from '@push.rocks/smartpromise';
import * as webrequest from '@push.rocks/webrequest';

export { isounique, lik, smartdelay, smartpromise, webrequest };
